package s0564513.meetingorganizer.exceptions;

public class EmptySpeakersListException extends Exception {

    public EmptySpeakersListException() {
    }

    public EmptySpeakersListException(String message) {
        super(message);
    }
}
