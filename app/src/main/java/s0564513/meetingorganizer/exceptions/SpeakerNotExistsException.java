package s0564513.meetingorganizer.exceptions;

public class SpeakerNotExistsException extends Exception {

    public SpeakerNotExistsException() {
    }

    public SpeakerNotExistsException(String message) {
        super(message);
    }
}
