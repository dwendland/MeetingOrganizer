package s0564513.meetingorganizer.model.memento;

import android.app.Activity;
import android.content.Context;

import s0564513.meetingorganizer.model.speaker.Speaker;

/**
 *
 */
public class SharedPrefsSpeakerMemento extends StringSpeakerMemento implements SpeakerMemento {

    private Context context;

    /**
     *
     * @param context
     */
    public SharedPrefsSpeakerMemento(Context context) {
        this.context = context;
    }

    /**
     *
     * @param speaker
     */
    @Override
    public void saveSpeaker(Speaker speaker) {
        ((Activity) context).getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString("speaker", this.speakerToString(speaker))
                .apply();
    }

    /**
     *
     * @return
     */
    @Override
    public Speaker getSpeaker() {
        return this.stringToSpeaker(((Activity) context).getPreferences(Context.MODE_PRIVATE)
                .getString("speaker", ""));
    }
}
