package s0564513.meetingorganizer.model.speaker;

import java.util.List;

import s0564513.meetingorganizer.model.memento.SpeakerListMemento;

public interface SpeakerList {
    List<Speaker> getSpeakers();
    boolean addSpeaker(Speaker speaker);
    boolean removeSpeaker(Speaker speaker);
    Speaker nextSpeaker();
    Speaker getSpeaker();
    int size();
    void saveToMemento(SpeakerListMemento memento);
    void restoreFromMemento(SpeakerListMemento memento);
}
